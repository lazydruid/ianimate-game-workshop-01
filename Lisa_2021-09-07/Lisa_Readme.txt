
LISA v43.0 (09—04-2021) iAnimate.net

--- v43 Updates ---
-Fixed ninja No-Gloves facial issues
-Fixed foot roll


--- v40 Updates ---
-IK FK switch now adjusted to work in python 3 (maya 2022) and works with scaled character
-shader correction for viewport

--- v37 Updates ---
-Adjusted picker so it works also in Maya 2022 and above

--- v36 Updates ---
-Locked all non-controllers channels to avoid rig breaking

--- v35 Updates --
-Fixed freeze of empty matrix

--- v34 Updates ---
-Altered arm/leg foot and pv constraints to follow "rig_grp" in "Root" mode instead of "rig_extra_2"
-Added "Root" space to weapon controls, also following rig_grp
-Fixed nostril

--- v31 Updates ---
Moved root_skl to world (to facilitate exports)
Adjusted rig to fix spine switching issue (script is the same, it's a rig-only fix)
Set the weapon toggle to hide the scabbard in both versions.
Skinned ninja facial rigging

--- v30 Updates ---
Rig:
-Removed containers from the rig
-Removed proxy attributes
-Optimize rotate order
-Add visibility options on the rig_grp
-Quality control pass
-Deleted keys / anim layers
-Add facial rig
-Removed Arnold attributes
-Thigh skinning optimisation
-Studio library issues

Picker:
-Adjusted pickers to the fixes above

Models:
-Fixed issue where users without Arnold sees the characters pitch black
-Shaders optimized for viewport, as well as using a separate shaders in Arnold renderer

--- v15 Updates ---

-Fixed prop bone not following the hand
-Delete node preventing to save in .ma


--- 4.1 Updates ---

-Added attribute to switch to Ninja
-Fix skinning issue on Ninja
-Unplugged Bumpmap as it was creating artefacts on the face



--- 4.0 Updates ---

- Whole rig re-done 
- Picker included


--- 3.6 Updates ---

- Proxy model fixed
- New arrows to indicate the direction of the main characters controllers, for visual purpose only.

--- 3.5 Updates ---

- New Ninja outfit available with attributes to change color scheme. 

--- 3.1 Updates ---

- All ctrl local axis have been fixed (Y=up, X=side, Z=depth)

--- 3.0 Updates ---

- Complete new rig Based on our new rig template, uptimized to Maya 2014


--- Using the iAnimateToolBox shelf for Select All, Key All and IK-FK match ---

Load the script "iAnimateToolBox" in the script editor (drag and drop it into the script editor) then select it all and  drag it with middle mouse button onto the shelf editor. That will create a new button in your shelf, click on it a a small window with both feature will appear.


--- Reconnect unlinked textures with the FileTextureManager script (each time you download a new version of the rig) ---

DOwnload the  FILE TEXTURE MANAGER script:
http://www.ianimate.net/ia25/index.php/rigs-a-resources/props-and-scripts/category/scripts

Move the FileTextureManager script file to Maya script folder

PC:  C:\Users\XXX\Documents\maya\scripts
Mac: \ComputerName\users\shared\Autodesk\maya\2012\scripts\

Run Maya and load your rig: (look at the "FileTextureManager.jpg" file for reference)

1. Type FileTextureManager in the MEL script command line (case-sensitive)
2. Press on "Analyse Scene File Textures"
3. Mark all the textures you want to reconnect
4. Browse and select the folder in which the textures are located (the sourceimages folder)
5. Click on "Seth Path"


